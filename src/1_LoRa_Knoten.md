
# LoRa Endgeräte

"LoRa Knoten" bzw. "Endgeräte" sind meist kleine batteriebetriebene Geräte die
Sensordaten in ein LoRaWAN Netzwerk schicken. Das TheThingsNetwork ist ein
frei zugängliches LoRaWAN Netz. Die Software zum Betreiben eines LoRaWAN Netzwerks ist der
"The Things Stack", es ist Open-Source Software mit der man auch ein eigenes
Netz realisieren kann (<https://github.com/TheThingsNetwork/lorawan-stack>).

Es gibt einige Möglichkeiten, zu einem Endgerät für LoRaWAN zu gelangen:

- Fertig kaufen: 

  TTN Marktplatz:
  <https://www.thethingsnetwork.org/marketplace/products/devices>

  Device Repository:
  <https://www.thethingsnetwork.org/device-repository>

  Reichelt:  Haus- und Sicherheitstechnik -> Home-Automation / Smart Home ->
  LoRaWAN (<https://www.reichelt.de/lorawan-c9039.html>)

- Selber bauen

  Siehe die folgenden Abschnitte


## Mikroprozessor plus Funkchip

![uP plus Funkchip](img/micro_lora.png)

Die LoRa Funkchips kommen von Semtech (<https://www.semtech.com>), z.B. SX127x
(1.  Generation) oder SX126x (2. Generation, stromsparender, mehr Leistung beim
Senden).  Die 1. Generation findet man noch immer fast überall, z.B. auf den
RFM95/RFM96 Modulen von HOPERF (siehe Abbildung). Beim Kauf von LoRa Modulen
sollte man darauf achten, dass sie für die in Europa standardisierten 868 MHZ
ausgelegt sind.

![RFM95](img/rfm95.jpg)

Die Pinbelegung stammt aus dem Manual von HOPERF, siehe
<https://cdn.sparkfun.com/assets/learn_tutorials/8/0/4/RFM95_96_97_98W.pdf>.

![RFM95-Pins](img/rfm95-pins.png)


Beispiele:

* Arduino Uno mit AVR Mega328 und einem "Dragino" Board, das über SPI mit dem
  AVR verbunden ist. Auf der Unterseite des roten Dragino Boards ist ein RFM95
  Modul aufgelötet.  Das war der allererste LoRa Versuch an der Hochschule,
  ca. 2017.

  ![Dragino](img/dragino.jpg)


* ESP32 + SX1276 auf "Heltec Wifi LoRa 32" Board. Es gibt mittlerweile die 
  Versionen V1, V2 und V3. Die Versionen unterscheiden sich teilweise in den 
  Anschlüssen. Die Stromsparqualität des ESP32 ist jedoch nicht besonders gut.
  Man tut sich schwer wenn man Geräte bauen will, die jahrelang aus einer
  kleinen Zelle mit z.B. 1 Ah laufen sollen.

  ![Heltec Board](img/heltec.png)


Vorteile

⊕ Die Programmierung erfolgt fast immer über das Arduino Framework. Ist also 
  auch für Bastler geeignet.

⊕ Auf dem Mikrocontroller läuft fast immer die LoRa "LMIC" Bibliothek für 
  Arduino (bzw. "MCCI LoRaWAN LMIC").

Nachteile

⊖ Man muss immer noch etwas C oder C++ können, damit man den Arduino Code versteht
  und ändern kann.

⊖ Manche häufig verwendeten Mikrocontroller wie der ESP32 sind nicht gut
  für Stromsparen ausgelegt.

⊖ LMIC Bibliothek kann z.B. nicht "Class C" Geräte.


## In Micropython programmierbare LoRa-Board

![LoPy Modul](img/lopy.jpg)

Die Abbildung zeigt das "LoPy" Modul der Firma PyCom (<https://pycom.io>), die
von Micropython-Erfinder Damien P. George gegründet wurde. Micropython an sich
(<https://www.micropython.org>) ist freie Software. Das LoPy ist im
wesentlichen auch nur ein ESP32 mit SX1276.

Vorteile

⊕ Sehr leichte Programmierung, es gibt nichts besseres für (Programmier)-Anfänger

⊕ Gute Dokumentation, z.B. <https://pycom.github.io/pydocs/datasheets/development/lopy.html> 

Nachteile

⊖ Es gibt (noch) keine freie Alternative für diese Boards

⊖ Die Module gibt es nur bei PyCom und haben einen stolzen Preis



## System-on-Chip aus Mikroprozessor und Funkchip

Dabei handelt es sich um Chips die bereits einen Mikrocontroller und einen 
Funkchip enthalten.  Die folgende Abb. zeigt ein "STM32WL55-Nucleo" Board, 
der obere IC in dem quadratischen Rahmen ist der STM32WL55. Die quadratische 
Fläche wird üblicherweise von einem Blechgehäuse verdeckt.

![STM32WL55-Nucleo](img/stm32wl55-nucleo.jpg)

<!--
STM32WL55 (ARM Cortex M4 + M0), STM32WLE55 (ARM Cortex M4), der Cortex M4
Kern entspricht einem STM32L476, dazu kommt ein SX127x Funkchip, beides ist 
in einen Baustein integriert.
-->

Den STM32WL gibt es auch in der Ausprägung STM32WLE55, er enthält nur einen 
Kern (Cortex M4), der WL55 hingegen hat zwei Kerne (Cortex M4 + M0). 

Fertig verwendbare Module, die mit dem STM32WLE55 ausgestattet sind:

- "dnt-TRX-ST1" Module von ELV, [Link](https://de.elv.com/elv-lw-base-experimentierplattform-fuer-lorawan-elv-bm-trx1-156514)

- "E5" Module von Seeed, [Link](https://wiki.seeedstudio.com/LoRa-E5_STM32WLE5JC_Module)

- "Lora868" Module von Olimex, [Link](https://www.olimex.com/Products/IoT/LoRa/LoRa868/open-source-hardware)


Die folgende Abb. zeigt das "ELV-LW-Base", das mit "dnt-TRX-ST1" ausgestattet
ist (<https://de.elv.com/lorawan>).

![ELV-LW-Base Basisboard](img/loris-base.jpg)


Vorteile

⊕ Sehr gute Stromspareigenschaften

Nachteile

⊖ Schwierigere Programmierung (ST-Micro CubeWL Bibliothek, eher was für
  erfahrene C Programmierer)


Ein paar Messungen an dem ELV-LW-Base ("LoRIS") Basisboard von ELV mit dem
dnt-TRX-ST1 Modul: 
<https://hhoegl.informatik.hs-augsburg.de/hhwiki/LoRIS>
(XXX leider aktuell hinter VPN)


Es wird bereits an einer Micropython Portierung dafür gearbeitet. Es kann gut
sein, dass man in einiger Zeit komplette LoRaWAN Knoten mit diesem Baustein 
in Python programmieren kann.




## LoRaWAN Module mit AT-Befehlssatz

Manche LoRaWAN Module können über die serielle Schnittstelle mit einem 
"AT-Befehlssatz" angesteuert werden. Eines der Ersten war das RN2483 von
Microchip (<https://www.microchip.com/en-us/product/RN2483>).

![RN2483](img/rn2483.jpg)

Der Befehlssatz wird in diesem Dokument beschrieben:
[DS40001784G](https://ww1.microchip.com/downloads/en/DeviceDoc/RN2483-LoRa-Technology-Module-Command-Reference-User-Guide-DS40001784G.pdf)

Es gibt auch Firmwares für die oben genannten Module auf der Basis des
STM32WL55, die AT Befehlssätze implementieren. Leider gibt es hier keine
Standardisierung der Befehle. Hier ist z.B. die Beschreibung für die AT 
Kommandos bei der Firmware für STM32WL55: [AN5481](https://www.st.com/resource/en/application_note/an5481-lorawan-at-commands-for-stm32cubewl-stmicroelectronics.pdf). Die gleiche Firmware funktioniert auch auf den ELV Modulen "dnt-TRX-ST1". 
Das "E5" Modul von Seeed hat bereits einen eingebauten AT Befehlssatz,
allerdings ist der auch wieder nicht zu anderen kompatibel.

Das RN2483 Modul ist z.B. auf dem "The Things UNO" drauf, einem Arduino der 
LoRaWAN kann (siehe <https://www.thethingsnetwork.org/marketplace/product/the-things-uno>).
Zum Einstieg kann ich TT-UNO sehr empfehlen, die Dokumentation ist 
gut für Anfänger geeignet (<https://www.thethingsindustries.com/docs/devices/the-things-uno>).


Vorteile

⊕ LoRaWAN Protokollsoftware bereits im Funkmodul enthalten

⊕ Beliebiger Mikrocontroller kann verwendet werden, z.B. einer auf dem 
  Micropython läuft (Raspberry Pico, BBC Micro:Bit, STM32, ...). 

⊕ Es gibt Software-Bibliotheken, die das Ansteuern der diversen AT Kommandos
  übernehmen.

⊕ Man kann mit dem Modul sofort interaktiv am PC arbeiten

Nachteile 

⊖ Keine Standardisierung des AT Befehlssatzes

⊖ Keine absolute Kontrolle über die Stromsparfunktionen




