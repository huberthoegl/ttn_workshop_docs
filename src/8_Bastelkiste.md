
# Aus der Bastelkiste

Auf dem Brett sieht man ein LoPy Board Version 1.0r
([Link](https://docs.pycom.io/products)).
Es ist aufgesteckt auf ein "Expansion Board" von Pycom. Damit man für
Anwendungen eine kleine Benutzerschnittstelle machen kann, sind zusätzlich ein
DOG-Display (2x16 Zeichen) und vier Tasten angeschlossen. Die Stromversorgung
erfolgt über eine 10 Ah USB Powerbank.

Das LoPy programmiert man komplett in der Interpretersprache Micropython. Neben
Versuchen mit Class-A Geräten habe ich auch Class-C darauf getestet. 

![LoPy-Brett](img/LoPy-Brett.jpg)


----

In dem kleinen Installationsgehäuse ist ein "STM32WL55-Nucleo" Board
([Link](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html)), das ich 
mit der AT-Kommando Firmware geflasht habe. Damit kann ich alle LoRaWAN 
Anweisungen als lesbare Kommandos über die serielle Schnittstelle schicken. Das 
schwarze USB Kabel "enthält" auch die serielle Schnittstelle. Die Box teste 
ich aktuell an einem Raspberry Pi 3, auf dem die Beregnungssteuerung der 
Plätze eines Tennisvereins läuft. Da die Steuerung keine Verbindung zum
Internet hat, möchte ich über LoRaWAN Class-C die wesentlichen Funktionen steuern
und den Status abfragen.

![STM32WL55-Box](img/stm32wl55-box.jpg)

----

In dem Installationsgehäuse (8 x 8 x 3 cm) befindet sich ein ELV-LW-Base Modul
([Link](https://de.elv.com/elv-lw-base-experimentierplattform-fuer-lorawan-elv-bm-trx1-156514)),
auf das ein ELV-AM-TH1 Applikationsmodul zum Messen von Temperatur und
Luftfeuchtigkeit gesteckt ist
([Link](https://de.elv.com/elv-temp-hum1-applikationsmodul-temperatur-und-luftfeuchte-elv-am-th1-157134)).
Die Stromversorgung erfolgt aus einer Li-Ion 18650 Zelle über einen kleinen
Schaltwandler von Pololu. Falls eine Zelle die Mindestspannung unterschreitet
muss ich sie ausbauen und in ein Ladegerät stecken. Bisher laufen die Boxen
aber seit 8 Monaten mit der ersten Ladung.  Optional kann man auch einen
externen Temperatursensor anschliessen (wird mitgeliefert, siehe schwarzes
Kabel). 

Mittlerweile habe ich vier dieser Boxen zu Hause in Betrieb um an verschiedenen
Stellen in einem alten Haus aus den 50er Jahren die Feuchtigkeit und Temperatur
im Auge zu behalten. Wenn man auch die Aussentemperatur und Feuchtigkeit 
einbezieht, kann man z.B. automatisiert über LoRa eine taupunktabhängige
Lüftung des Kellers realisieren.

![ELV-Loris-TH-Box](img/elv_loris_th_box.jpg)

---

Auf dem LilyGo T-Beam ist ein GPS Empfänger, so dass man schnell ein LoRaWAN
Gerät für den "TTN Mapper (<https://ttnmapper.org>) damit realisieren kann. Die 
Software findet man fertig im Netz und muss nur konfiguriert werden.

![LilyGo T-Beam](img/ttgo-gps.jpg)

