
# LoRa Endgerät mit Heltec WiFi LoRa 32 **V3**

Beim IoT Workshop in Dillingen verwenden wir das Heltec Board in der Version **V3**.


LoRa Gerät mit Heltec WiFi LoRa 32 V3

![Pinout V3](img/Pinout-V3.png)

[Zum Vergrössern klicken](img/Pinout-V3.png)



## Hardware 

![Aufbau V3](img/Aufbau-V3.jpg)

Die prinzipielle Schaltung ist identisch mit der Schaltung für Heltec V1. Nur die Belegung
der Pins ist anders:

- Der schwarze Taster ist active low und mit GPIO6 (J3, Pin 17) verbunden. 
  Vom Taster geht ein 10 kOhm Pullup Widerstand nach VCC (3.3V).

- Die Anode der roten LED ist über einen 390 Ohm Widerstand mit GPIO7 (J3, Pin 18)
  verbunden, ist also active high. Die Kathode ist mit Masse verbunden.  

- Ein blaue Potentiometer ist mit ADC Eingang ADC1_CH0 verbunden (J3, Pin 12, entspricht
  GPIO1). Der ADC hat eine 12-Bit Auflösung.  Die beiden Enden des Poti liegen 
  direkt an GND und 3.3 Volt. Bei der Schaltung mit Heltec V1 hatte ich Vorwiderstände
  verwendet, um mehr in den linearen Bereich zu kommen. Da müsste man nachsehen, wie 
  sich das Heltec V3 beim ADC Eingang verhält. 


Das Programm ist im Ordner `heltec_v3`.   


