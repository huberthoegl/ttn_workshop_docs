
# Workshop zu den Themen IoT, LoRaWAN und The Things Network

15./16. Juli 2024, Akademie in Dillingen

Hubert Högl, Technische Hochschule Augsburg (THA), <Hubert.Hoegl@tha.de>

URL: <https://www.tha.de/~hhoegl/iot_workshop>

<!--
TTN Community Augsburg: <https://www.thethingsnetwork.org/community/augsburg>
-->

<!-- geht aktuell leider nur ueber VPN
- <https://hhoegl.informatik.hs-augsburg.de/hhwiki/ttn> 
-->

---
**Neuigkeiten**

* Zusätzliches Material zum Kurs an der ALP in Dillingen (15./16. Juli 2024). Wer
  Fehler findet, Fragen oder Verbesserungsvorschläge hat kann mir gerne 
  eine Email schicken.

  <https://tha.de/~hhoegl/iot_workshop/alp24>

* Ich überarbeite diesen Text gerade, da manche Teile noch nicht das neue Heltec V3
  Board beschreiben. Das betrifft vor allem die Abschnitte "Heltec Board V3" und 
  "TTN konfigurieren". Die Demo für das Heltec V3 Board gibt es erst seit 
  14. Juli, sie ist im git Repo unter `ttn_workshop/heltec_v3/`.

---


Das Kursmaterial für den LoRaWAN Versuch auf dem Heltec Board ist im Gitlab 
Repository <https://gitlab.com/huberthoegl/ttn_workshop>. Man holt es auf den 
eigenen Rechner mit

```
git clone https://gitlab.com/huberthoegl/ttn_workshop
```

Ich mache gelegentlich Änderungen bei diesem Repository, sollten Sie es schon 
geklont haben, dann gelegentlich `git pull` eingeben, damit Sie auf dem neuesten
Stand sind.


Die im Text genannten Dateien und Ordner sind in diesem Repository.

Alle Beispiele werden unter Linux gezeigt.

<!--
Ich trage gerne Euren TheThingsNetwork-Anmeldenamen in meine "heltec-demo" 
Anwendung als "Collaborator" ein, dann seht ihr mein Anwendung in der Konsole.
-->

Sollte es nachträglich Fragen zu dem Workshop geben dann schreibt mir
gerne eine E-mail an `Hubert.Hoegl@tha.de`.


## Kurzübersicht der Themen

- Grobe Übersicht zu LoRa und LoRaWAN

  - Linux Infotag 2018 über LoRa 
    <https://www.tha.de/~hhoegl/doc/lora-lit18.pdf>
  
  - Vortrag an der Akademie in Dillingen 2021 (Online)
    <https://www.tha.de/~hhoegl/doc/Akademie/IoT-mit-LoRaWAN.html>

- Account auf <https://www.thethingsnetwork.org> besorgen.

- Wo ist das nächste Gateway? Siehe [Map](https://www.thethingsnetwork.org/map).

- Falls man kein Gateway in der Nähe hat: TheThingsNetwork Indoor Gateway (TTIG)

- LoRaWAN Gerät entweder selber bauen oder fertig kaufen

- Eine einfache Anwendung auf ein Heltec-Board bringen

- Payload kodieren und dekodieren

- Anwendung und Gerät in TTN Konsole konfigurieren

  Alternative: Command-Line Interface `ttn-lw-cli`

- Gerät schickt Daten, die man in der TTN Konsole sieht (Uplink)

  Von der TTN Konsole Daten an das Gerät schicken (Downlink)

- "Integration": Uplink und Downlink aus der Ferne am TTN Server steuern

  - Storage

  - Webhooks

  - MQTT 

  - Node-Red Dashboard
  
  - TTN Mapper
