
# Es läuft

Es läuft zum ersten Mal. Wir sehen dass das Gerät aktiv ist ("Last activity XXX
seconds ago").  Die Uplink Daten kommen im Reiter "Live data" in der Konsole
an. Man sieht die Payload in Form der einzelnen Byte `00 30 00 00 00` Wenn man
auf die Zeile mit den Daten klickt, dann öffnet sich ein Fenster mit einem
"riesigen" JSON Objekt, es enthält u.a. `frm_payload": "ADAAAAA="`, das ist die
Payload im Base64 Format.

Mit dem schon erwähnten Programm kann man diese Kodierung in Bytes umwandeln 
und erhält wie zu erwarten:

```
python base64tool.py -b ADAAAAA=
0x00  0x30  0x00  0x00  0x00
```

Über den Reiter "Messaging" kann man die Downlink-Daten in eine Queue
einreihen. Ohne Payload Formatierer muss man zunächst direkt den Byte-Wert 
eingeben (00 = LED aus, 01 = LED ein).

Altenativ kann man hier auch das CLI Programm `ttn-lw-cli` verwenden (siehe 
Abschnitt "TTN CLI":

Rote LED an

```bash
ttn-lw-cli end-devices downlink push heltec-demo-1 heltec-demo-dev1 --frm-payload 01 --priority NORMAL --f-port 1
```

Rote LED aus

```bash
ttn-lw-cli end-devices downlink push heltec-demo-1 heltec-demo-dev1 --frm-payload 00 --priority NORMAL --f-port 1
```

Wenn später die Payload Formatierung in die TTN Konsole eingebaut ist, kann
man den Uplink auch im dekodierten Format betrachten z.B. 
`{ adc: 1536, button: 0 }`. Auch den Downlink kann man dann über ein
dekodiertes JSON Objekt steuern, z.B.  `{ "led": "on" }`.


In Abschnitt 8 "Integration" sehen wir wie man mit anderen Mitteln Uplinks
abgreifen bzw. Downlinks verschicken kann.

- MQTT Client abonniert Uplink (subscribe)

- Uplink wird per Webhook nach Ziel-URL übertragen

- Downlink wird per Webhook an TTN Server gesendet


<!--
XXX to do 
MQTT publish (XXX to do)
--->


