
TheThingsNetwork Übersicht 

![TTN Übersicht](img/TTN-Uebersicht.svg)

[Zum Vergrössern klicken](img/TTN-Uebersicht.svg)



Gateways an der Hochschule 

- Gebäude E: Seit 2017 auf dem Dach des E-Technik Hochhauses (Imst Lite Gateway, [hsa-lora-gateway](https://wireless-solutions.de/products/lora-solutions-by-imst/development-tools/lite-gateway/))

- Gebäude J: Seit April 2023, für Workshop aufgebaut (RAK7258, [hsa-rak-gateway](https://www.rakwireless.com/en-us/products/lpwan-gateways-and-concentrators/rak7258), [Bild](img/RAK7258.jpg))

![Gateways](img/HSA-Lageplan.jpg)

[Zum Vergrössern klicken](img/HSA-Lageplan.jpg)
