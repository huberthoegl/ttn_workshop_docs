
# Kommandoreferenz

[Platformio](#platformio)  
[ttn-lw-cli](#ttn-lw-cli)  
[baes64tool.py](#base64toolpy)  
[MQTT](#mqtt)  
[Webhooks](#webhooks)  
[Node-RED](#node-red)   


## Platformio

* Installieren mit

  ```
  python3 -c "$(curl -fsSL https://raw.githubusercontent.com/platformio/platformio/master/scripts/get-platformio.py)"
  ```

  Es gibt danach den Ordner `.platformio` im Homeverzeichnis.


* Virtuelle Umgebung aktivieren

  ```
  . ~/.platformio/penv/bin/activate
  ```

  Nun sollte das Kommando `pio` gefunden werden, am besten mal `which pio` 
  eingeben und danach `pio --version`:

  ```
  which pio
  /home/hhoegl/.platformio/penv/bin/pio

  pio --version
  PlatformIO Core, version 6.1.6
  ```

* Nun in das `heltec` Verzeichnis mit dem Beispielprogramm gehen. Dann
  aufrufen:
 
  ```
  project init --board heltec_wifi_lora_32
  ```

  Danach gibt es im `heltec` Verzeichnis einen versteckten Ordner `.pio`. In
  diesem befindet sich z.B. die benötigte LoRa Bibliothek. Ausserdem wurde der 
  Ordner `.platformio` im Homeverzeichnis viel grösser (1,6G), da nun der 
  Compiler und das Arduino-Framework für das Heltec Board (ESP32) darin 
  installiert wurden.
  
* Code kompilieren mit 

  ```
  pio run 
  ```

* Code auf das Heltec Board übertragen mit 

  ```
  pio run -t upload
  ```

  Das angesteckte Heltec Board ist über ein USB Kabel mit dem PC verbunden. 
  Das USB Kabel dient der Stromversorgung und zur Übertragung der seriellen 
  Schnittstelle. Oft kommt man nur als Administrator (`root`) auf diese 
  Schnittstelle. Deswegen erst noch dem gewöhnlichen Anwender ($USER) den 
  Zugriff erlauben mit

  ```
  sudo usermod -a -G dialout $USER
  ```

  Danach ausloggen/einloggen oder komplett das Linux neu starten. Das Kommando
  `pio run -t upload` sollte automatisch das richtige serielle Gerät finden.


* Wenn man `pio` nicht mehr braucht deaktiviert man die virtuelle Umgebung 
  mit 

  ```
  deactivate
  ```



## ttn-lw-cli

Es gibt das Kommandozeilenprogramm `ttn-lw-cli`, das alle Funkionen der 
TTN Konsole auch über die Kommandozeile erledigen kann.  Es ist Teil des
The Things Stack und somit auch Open-Source Software.

<https://www.thethingsindustries.com/docs/the-things-stack/interact/cli>

Reference: <https://www.thethingsindustries.com/docs/reference/cli>

Download: <https://github.com/TheThingsNetwork/lorawan-stack/releases>


**Anmelden**

Version abfragen

```
ttn-lw-cli version

The Things Network Command-line Interface: ttn-lw-cli
Version:             3.25.1
Build date:          2023-04-18T08:13:17Z
Git commit:          bcfbbc610
Go version:          go1.20.3
OS/Arch:             linux/amd64
```


```
ttn-lw-cli use eu1.cloud.thethings.network
```

Lokal wird eine Datei `.ttn-lw-cli.yml` angelegt.

```
ttn-lw-cli login
```

Nun öffnet sich der Browser und zeigt den Auth-Code an. Diesen Code mit
Copy/Paste in das Kommandozeilenfenster übertragen.

Danach sollte jedes Kommando funktionieren, z.B.:

```
ttn-lw-cli applications list
```

```
ttn-lw-cli end-devices list heltec-demo
```

```
ttn-lw-cli gateways list
```


Zum Schluss 

```
ttn-lw-cli logout
```


**Downlink**

<https://www.thethingsindustries.com/docs/devices/downlink-queue-ops>

Downlink an Heltec Board schicken (1 Byte Nutzdaten)

Rote LED an

```bash
ttn-lw-cli end-devices downlink push heltec-demo eui-70b3d57ed005c674 --frm-payload 01 --priority NORMAL --f-port 1
```

Rote LED aus

```bash
ttn-lw-cli end-devices downlink push heltec-demo eui-70b3d57ed005c674 --frm-payload 00 --priority NORMAL --f-port 1
```

**DevNonce zurücksetzen**

Kann manchmal bei OTAA Geräten ganz praktisch sein ...

```bash
ttn-lw-cli end-devices reset heltec-demo eui-70b3d57ed005c674  --resets-join-nonces
```


## base64tool.py

Siehe Ordner `base64`.


```
python base64tool.py -B 01 3A
ATo=

python base64tool.py -b ATo=
0x01  0x3a

python base64tool.py -B 00
AA==
```


## MQTT

```
sudo apt install mosquitto
sudo apt install mosquitto-clients
pip install paho-mqtt
```

MQTTK

```
sudo apt install python3-tk
pip install mqttk
```



## Webhooks


```
# led-on.sh
curl --location \
     --header 'Authorization: Bearer '$WORKSHOP_API_KEY \
     --header 'Content-Type: application/json' \
     --header 'User-Agent: my-integration/my-integration-version' \
     --request POST \
     --data '{"downlinks":[{ "frm_payload": "AQ==", "f_port": 10, "priority":"NORMAL" }] }' \
     https://eu1.cloud.thethings.network/api/v3/as/applications/${appid}/webhooks/${webhookid}/devices/${devid}/down/replace
```

```
# led-off.sh
curl --location \
     --header 'Authorization: Bearer '$WORKSHOP_API_KEY \
     --header 'Content-Type: application/json' \
     --header 'User-Agent: my-integration/my-integration-version' \
     --request POST \
     --data '{"downlinks":[{ "frm_payload": "AA==", "f_port": 10, "priority":"NORMAL" }] }' \
     https://eu1.cloud.thethings.network/api/v3/as/applications/${appid}/webhooks/${webhookid}/devices/${devid}/down/replace
```



## Node-RED

* Man sollte zunächst Node.JS installieren  

  ```
  sudo apt install nodejs
  ```

  Deshalb erst mal testen ob man `node` schon hat:

  ```
  node
  Welcome to Node.js v12.22.9.
  Type ".help" for more information.
  > 
  ```

  Beenden mit `Strg-D`.

  Auch mal `npm` eingeben, der sollte mit Node.JS installiert worden sein. 


* Nun mit `npm` das Node-RED Programm installieren

  ```
  sudo npm install -g --unsafe-perm node-red
  ```

* Node-RED starten mit 

  ```
  node-red
  ```

  Das Kommando startet einen Server, der Webseiten ausliefert. Im Web-Browser
  unter `http://127.0.0.1:1880` findet man die Benutzerschnittstelle.

* In der Benutzerschnittstelle (Web-Browser) von Node-Red installieren (Menue
  -> Palette verwalten -> Installation). 

  - `node-red-dashboard`
  - `node-red-contrib-moment`

  Das Dashboard hat einen eigenen Zugangs-Link unter `http://127.0.0.1:1880/ui`

Unter <https://nodered.org/docs/getting-started/local> findet man auch noch 
andere Installationsmethoden.


