
# TTN konfigurieren

**Achtung: Dieser Abschnitt ist noch auf dem Stand des Heltec LoRa V1. Beim neuen Heltec V3 Board
gibt es einige Unterschiede, auf die ich im Kurs eingehen werde.** 

Dieser Abschnitt beschreibt die Arbeiten in der TheThingsNetwork Console

<https://eu1.cloud.thethings.network/console>


## Account besorgen

Gehe auf <https://www.thethingsnetwork.org> und wähle "Sign Up". 

Dann sieht man die folgende Seite im Browser:

![Join TTN](img/join-ttn.png)

Den linken Eintrag wählen (Join The Things Network). 



## TTIG Gateway (optional)

Erst mal schauen wo das nächste Gateway ist: <https://www.thethingsnetwork.org/map>

Hier sind die Augsburger Gateways (April 2023):

![Gateways in Augsburg](img/gateways-in-aux.png)

Falls man keines in der Nähe hat, empfehle ich das TTIG (TheThings Indoor
Gateway)

<https://www.thethingsindustries.com/docs/gateways/models/thethingsindoorgateway>


Das TTIG unterstützt 8 Kanäle, LoRaWAN 1.0.3  und hat im Inneren die Bausteine
SX1308 (Gateway) und einen ESP8266 für die WiFi-Anbindung. Das Gerät hat keine
Netzwerkbuchse. Normalerweise steckt man es einfach in eine Steckdose. Es hat
aber auch einen USB-C Anschluss zur Stromversorgung.

Hier ist die Prozedur für eine **Neuinitialisierung** des TTIG Gateways. 
Die angegebenen Daten (Wifi Pwd und Gateway-EUI) beziehen sich auf 
mein Gateway, diese Daten sollten bei Euch anders sein.

1.  5 Sek Reset druecken (LED blinkt rot und gruen)

2. 10 Sek Setup druecken (LED blinkt dann schnell rot)
  
   Nun ist das TTIG neu initialisiert. Über einen integrierten AP 
   bietet es das Netz mit der SSID MiniHub-80453A an.

3. Rechner mit WiFi Netz MiniHub-80453A verbinden 

   - Firefox verwenden! 
   - http://192.168.4.1
   - Wifi Pwd: oJurJyfz

   Die Website des Accesspoints zeigt auch alle wichtigen Daten des 
   TTIG an, siehe [TTIG-Daten](img/TTIG-Daten.png).

4. Konfigurieren, so dass TTIG beim nächsten Starten als WiFi Client 
   im Heimnetz ist.

5. Reboot (gruen sollte irgendwann dauerhaft leuchten, dann ist alles richtig
   verbunden).  An der Fritzbox sieht man dann den Netzteilnehmer 
   MiniHub-80453A.

6. Gateway in der TTN Konsole eintragen:

   Gateway-EUI 58 A0 CB FF FE 80 45 3A

  Der Claim Authentication Code ist das Wifi Pwd.


![Register Gateway](img/ttig_anmelden.png)


**Wichtig:** Bei mir war die im Bild gezeigte Gateway-ID schon vergeben,
deswegen wurde eine Fehlermeldung ausgegeben und ich konnte das Gateway erst
mal nicht anmelden. Man darf hier einen beliebigen Namen vergeben, ich habe
dann `huberts-ttig-gateway` genommen, damit hat es dann gleich funktioniert.

Siehe auch den Ordner `ttig`. 




## Applikation einrichten

Eine neue Applikation einzurichten geht sehr einfach. Man wählt lediglich `+
Create application` aus und vergibt eine neue Application ID, das ist ein
Bezeichner für die Applikation.  Ausserdem kann man einen Namen vergeben, 
den habe ich auf "Heltec Demo App for TTN Workshop" gesetzt (besser wäre 
aber ein kürzerer Name gewesen).

Sollte man die Applikation irgendwann wieder löschen, dann wird man bemerken,
dass man die gleiche ID nicht mehr vergeben kann. Anfangs habe ich für den
Workshop die ID `heltec-demo` vergeben, nachdem ich die Applikation zum Test
gelöscht und neu angelegt hatte, musste ich auf einen anderen Namen umsteigen,
z.B. `heltec-demo-1`.

So sieht der Fehler aus, wenn es den Namen früher schon gegeben hat:

![Applikation Fehler](img/create-application-failed.png)



## End Device einrichten

Das Anlegen eines Endgerätes (End Device) ist etwas umfangreicher. Man muss die
folgenden Punkte beachten:

* Daten manuell eingeben (da Selbstbaugerät)
* Frequency Plan
* LoRaWAN Spec 1.0.3
* JoinEUI auf `00 00 ...` setzen (war früher AppEUI)
* DevEUI generieren
* AppKey generieren 
* Abschliessend die End Device ID auf einen lesbaren Namen setzen, z.B.
  `heltec-demo-dev1`

Die DevEUI muss in der Reihenfolge "Little-Endian" übertragen werden, d.h. die
in der TTN Konsole gezeigte Reihenfolge der Bytes muss umgedreht werden!

Diese Folge von Bildschirmkopien zeigt das Anlegen eines Endgerätes. In Bild 10 
sieht man die umgedrehte Reihenfolge der DevEUI, so muss es in den Quelltext
eingegeben werden:

[1](img/register-end-device-1.png) |
[2](img/register-end-device-2.png) |
[3](img/register-end-device-3.png) |
[4](img/register-end-device-4.png) |
[5](img/register-end-device-5.png) |
[6](img/register-end-device-6.png) |
[7](img/register-end-device-7.png) |
[8](img/register-end-device-8.png) |
[9](img/register-end-device-9.png) |
[10](img/register-end-device-10.png) 




## API Keys


Wenn man aus einem Shell Skript, einem Python Programm oder einem 
Programm wie z.B. `curl` auf die Daten des TTN Servers zugreifen will, dann 
braucht man immer einen API Key. Den Key muss man in der TTN Konsole erzeugen 
(`+ Add API Key`). Er erhält einen Namen und einen Wert. Der Name ist in 
meinem Beispiel "heltec-demo-api-key", der Wert ist nur direkt bei der 
Erzeugung sichtbar und muss gesichert werden. Nach Abschluss ist der Wert
nicht mehr zugänglich. 

Der Key könnte in etwa so aussehen, muss aber natürlich an den eigenen Key 
angepasst werden:

```
NNSXS.ABS3TTW5OBMNZRBIW5ZMQDEC74N63JXYPAWAHVQ.GXO3XHNDPBYM2NW5I5ZYAMX6WNF3KRTX2FUW2YTFVXJXPVYFAGUQ
```

Die Demo-Programm für den Workshop lesen diesen API Key aus einer
Umgebungsvariable `WORKSHOP_API_KEY` ein. Diese muss deshalb erst mal 
gesetzt werden. Das macht man mit dem folgenden Kommando in der Shell:

```
export WORKSHOP_API_KEY=NNSXS.ABS3TTW5OBMNZRBIW5ZMQDEC74N63JXYPAWAHVQ.GXO3XHNDPBYM2NW5I5ZYAMX6WNF3KRTX2FUW2YTFVXJXPVYFAGUQ
```

Dadurch bleiben die Skripte (1) unabhängig vom Schlüssel und können (2) auf
öffentlichen Git Servern aufbewahrt werden, ohne dass Geheimnisse weitergegeben
werden.

Die Key ID ist lediglich ein eindeutiger Bezeichner für den Schlüssel. Auf den
Wert des Schlüssels kann nicht mehr zugegriffen werden. Sollte der Wert
verloren gehen, muss man einen neuen Schlüssel erzeugen.

Hier sind eine Reihe von Bildschirmkopien die zeigen wie der API Key
erzeugt wurde:

[1](img/api-key-1.png) |
[2](img/api-key-2.png) |
[3](img/api-key-3.png) |
[4](img/api-key-4.png) |
[5](img/api-key-5.png)


