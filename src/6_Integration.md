
# Integration

[MQTT](#mqtt)  
[Webhooks](#webhooks)  
[Storage](#storage)    
[Node-RED](#node-red)  
[Mapper](#mapper)  


Bisher haben wir nur interaktiv über die TTN Konsole Daten aus
Upstream-Richtung betrachtet und in Downstream-Richtung eingegeben. Nun ist es
das Ziel, dass man mit Programmen die auf dem eigenen Rechner laufen auf den
TheThings Netzwerkserver zugreift, so dass man die TTN Konsole eigentlich nicht
mehr braucht.  Der Netzwerkserver bietet dafür die "Integrations". Die zwei
wichtigsten grundlegenden Integrationen sind MQTT und Webhooks. Über diese 
Standards kann man Uplink und Downlink vom eigenen Rechner aus erreichen.

Die Uplink-Daten gehen nach dem Empfang im Netzwerkserver verloren, man sieht
sie vielleicht noch in der Konsole, kann aber nicht mehr darauf zugreifen. Die
Integration "Storage" speichert die empfangenen Daten eine Weile (24 Stunden)
und man kann sie über einen HTTP Endpunkt lesen.

Als nächstes stelle ich kurz Node-RED vor, keine richtige Integration, sondern
ein Werkzeug für grafische Programmierung. Es kann mit dem TTN Server über MQTT
und Webhooks interagieren.

Zum Schluss zeige ich den TTN Mapper. Das ist eine vorgefertigte
Webhook-Integration, mit der man die ungefähre LoRa-Abdeckung in einem Gebiet
ermitteln kann.



## MQTT

<https://www.thethingsindustries.com/docs/integrations/mqtt>

User: `heltec-demo-1@ttn`

Topic: `v3/<user>/devices/<device-id>/up`

Server: `eu1.cloud.thethings.network`, Port 1883 (unsafe), 8883 (safe)


Drei Demo-Clients:

- Python: `mqtt/client.py` 

  Paho-MQTT muss evtl noch installiert werden mit `pip install paho-mqtt`.

- Mosquitto commandline client: `mqtt/mosquitto.sh`. Aufrufen mit 
  `bash mosquitto.sh`.

  Installation:

  ``` 
  sudo apt install mosquitto
  sudo apt install mosquitto-clients
  ```

- Node-Red (siehe Abschnitt ganz unten)


MQTTK: Simples Tool in Python mit Tkinter GUI

![MQTTK](img/mqttk.png)

Die Installationsanweisungen auf meinem Linux Mint waren:

```
sudo apt install python3-tk
pip install mqttk
```



## Webhooks

Auf "Webhooks" basieren jede Menge Dienste, die meist von externen
kommerziellen Dienstleistern stammen. Wenn man bei der Webhook Integration auf
`+ Add webhook` klickt, dann sieht man sofort eine Menge Dienste, hier sind
Bildschirmkopien: [Webhooks1](img/webhook-icons-1.png) | [Webhooks2](img/webhook-icons-2.png). 
Fast alle sind externen Dienstleister die meist einen kostenlosen
Einstiegstarif anbieten, oft ist dieser aber in irgend einer Form beschränkt. 

Der Dienst ganz links oben auf [Webhooks1](img/webhook-icons-1.png)  ist der 
"Custom Webhook" den man für eigene Zwecke konfigurieren kann.

Die Anwendung `heltec-demo` hat eine solche Custom Webhook Integration bekommen. 

![webhook](img/custom-webhook.png)

Nun klickt man auf die Zeile `heltec-demo-wh`, danach sieht man die
Einstellungen die man bei diesem Webhook vornehmen kann:

![Webhook Konfiguration](img/webhook-config.png)

Die Webhook-ID ist `heltec-demo-wh`. Als einzig wichtigen Parameter muss man die
Base URL angeben, diesen habe ich auf <https://webhook.site> gesetzt. Das ist 
ein praktischer Dienst, mit dem man untersuchen kann, welche Daten ein Webhook 
tatsächlich abliefert.  Siehe `webhooks/README.md`. 

Im produktiven Einsatz würde man jedoch einen eigenen Webhook Server betreiben,
der vom TTN Server die HTTP Webhook-Aufrufe erhält und der die im JSON Format
übermittelten Daten abspeichert, auswertet, etc. Eine gut funktionierende
Software für diesen Zweck ist der `webhookd`, siehe 
<https://github.com/ncarlier/webhookd>.


Die typischen Anwendungsfälle sind:

- Uplink-Daten vom TTN Server auf einen anderen Rechner übertragen

- Downlink-Daten von einem anderen Rechner auf den TTN Server übertragen



### Uplink über Webhooks

Den eingerichteten Webhook kann man editieren ("Edit webhook"). Bei "Enabled
event types" klickt man auf "Uplink message" und gibt den Pfad an, der von 
`webhook.site` vorgegeben wurde (z.B. `/a9242d1b-ebfb-42b4-b0ec-cd4f650437af`)
Daraufhin sieht man den Uplink in `webhook.site`:

![https://webhook.site](img/webhook.site.png)

[zum Vergrössern klicken](img/webhook.site.png)


### Downlink über Webhooks

Siehe im Verzeichnis `webhooks` die bash Skripte `led-on.sh` und `led-off.sh`.
Sie übertragen mit dem `curl` Tool die Downlink-Daten zum TTN Server über 
HTTP POST Requests. 




## Storage

<https://www.thethingsindustries.com/docs/integrations/storage>

Kann man die Storage Integration für die Applikation oder individuell für ein
Gerät in der Applikation ein- oder ausschalten. Für `heltec-demo` ist sie
aktuell aktiviert.

Nicht zu viel erwarten, die Daten kommen nicht in Echtzeit an und werden auch 
nur eine bestimmte Zeit (24 Stunden) aufgehoben.

Die Daten bekommt man von den Endpunkten:

```
GET https://eu1.cloud.thethings.network/api/v3/as/applications/heltec-demo/packages/storage/{type}
GET https://eu1.cloud.thethings.network/api/v3/as/applications/heltec-demo/devices/{device_id}/packages/storage/{type}
```

Siehe das Verzeichnis `storage` im Git Repository. Abrufen der gespeicherten
Einträge z.B. mit `curl`, siehe `bash get-storage.sh`, oder besser formatiert
mit `bash get-storage.sh | jq . | less`.



## Node-Red

<https://www.thethingsindustries.com/docs/integrations/node-red>

Node-RED (<https://nodered.org>) ist eine "low code" Programmierumgebung, in
der man einfache Datenverarbeitungsabläufe grafisch im Web-Browser zeichnen
kann. Man setzt Knoten (Nodes) in ein Zeichenfeld und verbindet die Knoten mit
geschwungenen Linien.  Für die Anbindung an den TheThings Network Server gibt 
es z.B. einen MQTT Knoten, den man mit den Verbindungsdaten konfigurieren kann.


Installation mit `npm` Paketmanager von Node.JS:

```
sudo npm install -g --unsafe-perm node-red
```

In Node-Red installieren (Menue -> Palette verwalten -> Installation). 

- node-red-dashboard
- node-red-contrib-moment


Node-RED Server starten mit `node-red`.  Man bedient Node-RED über den Web
Browser mit folgendem Link:

```
http://127.0.0.1:1880
```

Das Dashboard hat einen eigenen Link:

```
http://127.0.0.1:1880/ui
```

Mit der Dashboard-Erweiterung bekommt man neue Knoten in der Kategorie 
"Dashboard", z.B. Text, Gauge und Chart.

Bildschirmkopie: [Dashboard mit Poti](img/node-red-heltec-poti.png)

Der dazugehörige Node-RED "Flow" ist im Verzeichnis `node-red` in der 
Datei `heltec-flow.json` enthalten.



# Mapper

Der TTN Mapper ist eine Web Anwendung unter <https://ttnmapper.org>. Man
benötigt ein LoRa Endgerät, das über ein GPS Modul verfügt. Die
Endgerätesoftware übermittelt periodisch die aktuellen GPS Koordinaten an den
TTN Server. Die Applikation (oder das Endgerät) auf dem Server muss mit der
vorgefertigten Webhook-Integration "TTN Mapper" verknüpft werden. Dadurch
werden die empfangenen Daten inklusive des GPS Standortes auf `ttnmapper.org`
per HTTP Push weitergereicht. 

Bildschirmkopien:

* [Webhook Konfiguration](img/ttnmapper-webhook-config.png)

* Auf `ttnmapper.org`:

  - [Menüpunkt Advanced Maps](img/ttnmapper-adv-maps-1.png) Die gezeigte 
    Device ID `eui-70b3d57ed004c4a0` gehört meinem Mapper Endgerät.

  - [Radfahrt am 22.4.](img/ttnmapper-22.4.2023-b.png) Von zu Hause zur
    Hochschule und wieder zurück.

  - [Radfahrt/Spaziergang am 23.4.](img/ttnmapper-23.4.2023.png) Von zu 
    Hause, Pfersee, Bahnhof, Innenstadt (Annastrasse), Rathaus, Maxstrasse,
    Wintergasse, Predigerberg, Bäckergasse, Puppenkiste, Hochschule und zurück.
    In einem Grossteil der Innenstadt gab es leider keine LoRa Verbindung zu
    einem Gateway. Erst ab der Gegend der Bäckergasse hat man wieder Empfang
    durch die Hochschul-Gateways und das "Maiskolben"-Gateway.


Der URL beim TTN Mapper kodiert die Device-ID, Start- und Enddatum. 

```
https://ttnmapper.org/devices/?device=eui-70b3d57ed004c4a0&startdate=2023-04-23&enddate=2023-04-23&gateways=on&lines=on&points=on
```

