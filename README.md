# ttn_workshop_docs

Den Text kann man mit `mdbook` [1] in statische HTML Seiten verwandeln.
Es ist ein in Rust geschriebenes Programm, das man wie folgt aufruft:

Erzeugen der HTML Seiten

```
mdbook build
```

Danach anschauen der HTML Seiten mit `firefox book/index.html`

Entfernen der HTML Seiten

```
mdbook clean
```

[1] Doku <https://rust-lang.github.io/mdBook/index.html>, Quelltext und 
Releases <https://github.com/rust-lang/mdBook>
